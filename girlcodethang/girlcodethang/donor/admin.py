from django.contrib import admin

from .models import donor, money

admin.site.register(donor)
admin.site.register(money)