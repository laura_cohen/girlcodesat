from django.db import models


class volunteerOne(models.Model):
    name = models.CharField(max_length=200)
    age = models.IntegerField(default = 0)
    def __str__(self):
        return self.name


class helpOne(models.Model):
    #foreign key tells us that each help is related to a single volunteer
    #supports many1, MM, 11
    question = models.ForeignKey(volunteerOne, on_delete = models.CASCADE)
    activty = models.CharField(max_length=200)
    def __str__(self):
        return self.activity
