from django.urls import path
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.contrib.auth.views import login
from django.conf.urls import url



from welcome import views

urlpatterns = [
    path('', views.index, name='index'),
   # url(r'^login/$', auth_views.login, name='login'),
   # url(r'^logout/$', auth_views.logout, name='logout'),
    #url(r'^admin/', admin.site.urls),

    url(r'^login/$', login),

    url(r'^register/$', views.UserFormView.as_view(), name='register'),
    url(r'^signup/$', views.UserFormView.as_view(), name='signup'),


]


