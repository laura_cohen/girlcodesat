from django.db import models

class volunteer(models.Model):
    name = models.CharField(max_length=200)
    age = models.IntegerField(default = 0)

    def __str__(self):
        return self.name


class help(models.Model):
    #foreign key tells us that each help is related to a single volunteer
    #supports many1, MM, 11
    question = models.ForeignKey(volunteer, on_delete = models.CASCADE)
    activty = models.CharField(max_length=200)

    def __str__(self):
        return self.activity

#makemigrations "python manage.py makemigrations polls" tells
#us that weve made changed to our model and that youd like to store them
#as a migrations.


#migrate command takes migrations that havent been applied and runs them to database to synch
# allows to change wihtout deleting over time and losing data