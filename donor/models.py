from django.db import models
from django.utils import timezone

now = timezone.now()


class Donor(models.Model):
    name = models.CharField(max_length=200)
    age = models.IntegerField(null=True)
    organisation = models.CharField(max_length=500, blank=True)
    emailAddress = models.CharField(max_length=500)
    history = models.TextField(max_length=20000, blank=True)
    id = models.IntegerField(primary_key=True)

    def __str__(self):
        return self.name


class Money(models.Model):
    # foreign key tells us that each help is related to a single volunteer
    # supports many1, MM, 11
    question = models.ForeignKey("donor", on_delete=models.CASCADE)
    activty = models.CharField(max_length=200)

    def __str__(self):
        return self.activty


class Donations(models.Model):
    now = timezone.now()
    CATEGORY = [('A', 'Education'), ('B', 'Recycling'), ('C', 'Clean Water and Sanitation'),
                ('D.', 'Health and Well-being'), ('E.', 'Wildlife')]
    donationsMade = models.ManyToManyField('Donations', blank=True, null=True)
    donationDate = models.DateTimeField(default=now)
    donationType = models.CharField(choices=CATEGORY, max_length=50)



