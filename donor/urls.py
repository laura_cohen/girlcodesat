from django.urls import path

from . import views

urlpatterns = [
     path('', views.index, name='index'),

   path('index', views.index, name='navIndex'),
    path('donate', views.donate, name='donate'),
    path('profile', views.profile, name = "profile"),
     path('dashboard', views.dashboard, name='dashboard'),
     path('uploads', views.uploads, name='uploads'),
 ]
