from django.http import HttpResponse

from django.shortcuts import render


 #return HttpResponse(template.render(context, request))
def index(request):
    return render(request, 'donor/index.html')

def donate(request):
    return render(request, 'donor/donate.html')


  # return HttpResponse(template.render(context, request))

  # return HttpResponse('Donate')

def profile(request):

    return render(request, 'donor/profile.html')

def uploads(request):
    return render(request, 'donor/uploads.html')
    #return render(request, "volunteer/info.html", context)tpResponse("here we will have your overview")


def dashboard(request):
    return render(request, 'donor/dashboard.html')