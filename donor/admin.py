from django.contrib import admin

from .models import Donor, Money, Donations

admin.site.register(Donor)
admin.site.register(Money)