from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('index', views.index, name='navIndex'),
    path('profile', views.profile, name="profile"),
    path('leaderbord', views.leaderbord, name='leaderbord'),
    path('upload', views.upload, name='upload'),
    path('uploadConf', views.uploadConf, name='uploadConf'),
]