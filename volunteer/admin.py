from django.contrib import admin

from .models import volunteerOne, helpOne

admin.site.register(volunteerOne)
admin.site.register(helpOne)