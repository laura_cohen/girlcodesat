from django.http import HttpResponse
from django.views.generic import TemplateView
from django.shortcuts import render


# from volunteer.forms import UploadForm


def index(request):
    # return HttpResponse("Hello, world. You're at the polls index.")
    return render(request, 'volunteer/index.html')


def profile(request):
    return render(request, 'volunteer/profile.html')


def leaderbord(request):
    return render(request, 'volunteer/leaderbord.html')


def upload(request):
    return render(request, 'volunteer/upload.html')


def uploadConf(request):
    return render(request, 'volunteer/uploadConf.html')

# def upload(request):
#     upPage= 'volunteer/upload.html'

#     def get(self, request):
#         form = UploadForm()
#         return render(request, self.upPage, {"form":form})



